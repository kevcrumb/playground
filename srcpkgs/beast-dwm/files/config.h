/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "monospace:size=10" };
static const char dmenufont[]       = "monospace:size=10";
static const char col_black[]       = "#000000";
static const char col_yellow[]      = "#ffff00";
static const char col_gray1[]       = "#222222";
//static const char col_gray2[]       = "#444444";
//static const char col_gray3[]       = "#bbbbbb";
//static const char col_gray4[]       = "#eeeeee";
//static const char col_cyan[]        = "#005577";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_yellow, col_black, col_gray1 },
	[SchemeSel]  = { col_black, col_yellow, col_yellow },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */

        // 1 << 8: 1 (one) shifted to the left by eight positions generates mask 10000000,
        // thus selecting tag ‘9’ (ninth from the right) in the the tags array.
        // Details: http://dwm.suckless.org/customisation/tagmask
	// NOTE: Apparently the instance name has to be used in the "class" column here.
	// Use  xprop | awk '/WM_CLASS/{ print $4 }'  to find the string to provide.
	/* class          instance    title       tags mask     isfloating   monitor */
	{ "Alacritty",    NULL,       NULL,       0,            0,           -1 },
	{ "URxvt",        NULL,       NULL,       0,            0,           -1 },
     // { "Deluge",       NULL,       NULL,       1 << 1,       0,           -1 },
     // { "Shotcut",      NULL,       NULL,       1 << 1,       0,           -1 },
     // { "google-chrome",NULL,       NULL,       1 << 2,       0,           -1 },
     // { "chrome",       NULL,       NULL,       1 << 2,       0,           -1 },
     // { "Chromium",     NULL,       NULL,       1 << 2,       0,           -1 },
     // { "Firefox",      NULL,       NULL,       1 << 2,       0,           -1 },
     // { "Tor Browser",  NULL,       NULL,       1 << 2,       0,           -1 },
     // { "Keepassx",     NULL,       NULL,       1 << 2,       0,           -1 },
     // { "Sylpheed",     NULL,       NULL,       1 << 3,       0,           -1 },
        { "Gimp",         NULL,       NULL,       1 << 7,       0,           -1 },
     // { "Conky",        NULL,       NULL,       1 << 8,       1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.618; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_black, "-nf", col_yellow, "-sb", col_yellow, "-sf", col_black, NULL };
static const char *termcmd[]  = { "alacritty", NULL };
static const char *lock_screen[]  = { "sh", "-c", "beast_desktop_lock_screen || i3lock || slock", NULL };
static const char *lock_and_suspend_to_ram[]  = { "sh", "-c", "beast_desktop_lock_screen && echo deep | sudo tee /sys/power/mem_sleep && echo mem | sudo tee /sys/power/state", NULL };
static const char *paste_snippet[]  = { "sh", "-c", "beast_desktop_paste_snippet", NULL };

// Put this in /etc/sudoers file: /usr/bin/nohup /bin/sh -c touch /run/nologin && sleep 5 && poweroff
static const char *poweroff[]  = { "sh", "-c", "sudo /usr/bin/nohup /bin/sh -c 'touch /run/nologin && sleep 5 && poweroff' & (sleep 1 && wait `pidof touch` && killall --user ${USER})", NULL };

// Put this in /etc/sudoers file: /usr/bin/nohup /bin/sh -c touch /run/nologin && sleep 5 && reboot
static const char *reboot[]  = { "sh", "-c", "sudo /usr/bin/nohup /bin/sh -c 'touch /run/nologin && sleep 5 && reboot' &> /dev/null & (sleep 1 && wait `pidof touch` && killall --user ${USER})", NULL };

/* Hotkeys - set XDG_CONFIG_HOME in xinitrc */
// TODO move these two into hotkey scripts
//static const char *start_xev[]  = { "st", "-f", "monaco:size=19", "-t", "xev" , "-e", "xev", NULL };
//static const char *start_xzoom[]  = { "xzoom", NULL };
static const char *execute_f1_action[]   = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F1.sh",  NULL };
static const char *execute_f2_action[]   = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F2.sh",  NULL };
static const char *execute_f3_action[]   = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F3.sh",  NULL };
static const char *execute_f4_action[]   = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F4.sh",  NULL };
static const char *execute_f5_action[]   = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F5.sh",  NULL };
static const char *execute_f6_action[]   = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F6.sh",  NULL };
static const char *execute_f7_action[]   = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F7.sh",  NULL };
static const char *execute_f8_action[]   = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F8.sh",  NULL };
static const char *execute_f9_action[]   = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F9.sh",  NULL };
static const char *execute_f10_action[]  = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F10.sh", NULL };
static const char *execute_f11_action[]  = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F11.sh", NULL };
static const char *execute_f12_action[]  = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F12.sh", NULL };


/* Audio (mpd) - set MPD_REMOTE_HOST in xinitrc */
static const char *remote_mpd_next[]             = { "sh", "-c", "mpc --host \"${MPD_REMOTE_HOST:=localhost}\" next", NULL };
static const char *remote_mpd_prev[]             = { "sh", "-c", "mpc --host \"${MPD_REMOTE_HOST:=localhost}\" prev", NULL };
static const char *remote_mpd_stop[]             = { "sh", "-c", "mpc --host \"${MPD_REMOTE_HOST:=localhost}\" stop", NULL };
static const char *remote_mpd_seek_back10[]      = { "sh", "-c", "mpc --host \"${MPD_REMOTE_HOST:=localhost}\" seek -10", NULL };
static const char *remote_mpd_seek_back30[]      = { "sh", "-c", "mpc --host \"${MPD_REMOTE_HOST:=localhost}\" seek -30", NULL };
static const char *remote_mpd_seek_forward20[]   = { "sh", "-c", "mpc --host \"${MPD_REMOTE_HOST:=localhost}\" seek +20", NULL };
static const char *remote_mpd_seek_forward30[]   = { "sh", "-c", "mpc --host \"${MPD_REMOTE_HOST:=localhost}\" seek +30", NULL };
static const char *remote_mpd_toggle[]           = { "sh", "-c", "mpc --host \"${MPD_REMOTE_HOST:=localhost}\" toggle", NULL };
static const char *remote_mpd_decrease_volume[]  = { "sh", "-c", "mpc --host \"${MPD_REMOTE_HOST:=localhost}\" volume -5", NULL };
static const char *remote_mpd_increase_volume[]  = { "sh", "-c", "mpc --host \"${MPD_REMOTE_HOST:=localhost}\" volume +5", NULL };

/* Audio - if MPD_HOST is set (e.g. in xinitrc), mpc will use it */
static const char *mpv_or_mpd_next[]             = { "sh", "-c", "[ `pidof mpv` ] && (jo command=`jo -a -- playlist-next` | socat - /tmp/mpvsocket) || mpc next", NULL };
static const char *mpv_or_mpd_prev[]             = { "sh", "-c", "[ `pidof mpv` ] && (jo command=`jo -a -- playlist-prev` | socat - /tmp/mpvsocket) || mpc prev", NULL };
static const char *mpv_or_mpd_stop[]             = { "sh", "-c", "[ `pidof mpv` ] && (jo command=`jo -a -- stop` | socat - /tmp/mpvsocket) || mpc stop", NULL };
static const char *mpv_or_mpd_seek_back10[]      = { "sh", "-c", "[ `pidof mpv` ] && (jo command=`jo -a -- seek -10` | socat - /tmp/mpvsocket) || mpc seek -10", NULL };
static const char *mpv_or_mpd_seek_back30[]      = { "sh", "-c", "[ `pidof mpv` ] && (jo command=`jo -a -- seek -30` | socat - /tmp/mpvsocket) || mpc seek -30", NULL };
static const char *mpv_or_mpd_seek_forward20[]   = { "sh", "-c", "[ `pidof mpv` ] && (jo command=`jo -a -- seek 20` | socat - /tmp/mpvsocket) || mpc seek +20", NULL };
static const char *mpv_or_mpd_seek_forward30[]   = { "sh", "-c", "[ `pidof mpv` ] && (jo command=`jo -a -- seek 30` | socat - /tmp/mpvsocket) || mpc seek +30", NULL };
static const char *mpd_show_current_id3[]        = { "sh", "-c", "beast_audio_display_id3_in tmux", NULL };
static const char *mpv_or_mpd_toggle[]           = { "sh", "-c", "[ `pidof mpv` ] && (jo command=`jo -a keypress space` | socat - /tmp/mpvsocket) || mpc toggle", NULL };
static const char *decrease_volume[]             = { "sh", "-c", "amixer set Master,0 5%- unmute", NULL };
static const char *increase_volume[]             = { "sh", "-c", "amixer set Master,0 5%+ unmute", NULL };
// With some devices sound is only heard if all three of 'Master', 'Headphone' and 'Speaker' are set to unmute.
// Conversely only 'Master' has to be muted to silence everything.
// So volume_mute always ensures that 'Headphone' and 'Speaker' are unmuted while 'Master' is actually toggled on/off.
static const char *volume_mute[]                 = { "sh", "-c", "amixer set Master toggle && for c in Headphone Speaker; do amixer set $c unmute; done", NULL };

// TODO When backlight brightness is at maximum add xrandr-based brightness for bad lighting conditions.
// Concept:
// outputs=`xrandr -q | grep --word-regexp connected | grep -E '[0-9]x[0-9]' | awk '{ print $1 }' 2>/dev/null`
// echo $outputs| while read line ; do xrandr --output $line --brightness 1 ; done
static const char *increase_brightness[]  = { "zsh", "-c", "expr `cat /sys/class/backlight/*/brightness` + 87 | sudo tee /sys/class/backlight/*/brightness", NULL };
static const char *decrease_brightness[]  = { "zsh", "-c", "expr `cat /sys/class/backlight/*/brightness` - 87 | sudo tee /sys/class/backlight/*/brightness", NULL };
static const char *increase_opacity[]     = { "sh", "-c", "transset-df --actual --inc 0.05 --max 1.00", NULL };
static const char *decrease_opacity[]     = { "sh", "-c", "transset-df --actual --dec 0.05 --min 0.10", NULL };

/* Jump to or start applications */
// Consider adding "xdotool search --sync --limit=1 --classname <XYZ> windowactivate --sync" _after_ starting new apps - in case they're configured to open on a different tag than the current one.
static const char *jump_start_browser[]      = { "sh", "-c",
	"xdotool search --limit=1 --classname 'Navigator|Tor|Launcher|chromium|firefox|google-chrome' windowactivate --sync ||"
	"( tor-browser || firefox --private-window || chromium --incognito || google-chrome --incognito ) &", NULL };
//static const char *jump_start_cantata[]      = { "sh", "-c", "xdotool search --limit=1 --classname cantata windowactivate --sync || cantata", NULL };
//static const char *jump_start_chrome[]       = { "sh", "-c", "xdotool search --limit=1 --classname chromium windowactivate --sync || xdotool search --limit=1 --classname google-chrome windowactivate --sync || chromium --incognito || google-chrome --incognito", NULL };
//static const char *jump_start_conky[]        = { "sh", "-c", "xdotool search --limit=1 --classname conky windowactivate --sync || conky -D", NULL };
//static const char *jump_start_deluge[]       = { "sh", "-c", "xdotool search --limit=1 --classname deluge windowactivate --sync || deluge", NULL };
//static const char *jump_start_firefox[]      = { "sh", "-c", "xdotool search --limit=1 --classname firefox windowactivate --sync || firefox", NULL };
//static const char *jump_start_gmpc[]         = { "sh", "-c", "xdotool search --limit=1 --classname gmpc windowactivate --sync || gmpc", NULL };
//static const char *jump_start_keepassx[]     = { "sh", "-c", "xdotool search --limit=1 --classname keepassx windowactivate --sync || keepassx", NULL };
//static const char *jump_start_libreoffice[]  = { "sh", "-c", "xdotool search --limit=1 --classname libreoffice windowactivate --sync || libreoffice", NULL };
static const char *jump_start_mpv[]          = { "sh", "-c",
	"xdotool search --limit=1 --classname mpv windowactivate --sync ||"
	" cd \"`xdg-user-dir SXIV`\" && find ./ -type f -maxdepth 1 | sort --random-sort --random-source=/dev/urandom | mpv --playlist=-", NULL };
//static const char *jump_start_mupdf[]        = { "sh", "-c", "xdotool search --limit=1 --classname mupdf windowactivate --sync || mupdf", NULL };
static const char *jump_start_shotcut[]      = { "sh", "-c", "xdotool search --limit=1 --classname shotcut windowactivate --sync || shotcut", NULL };
static const char *jump_start_surf[]         = { "sh", "-c", "xdotool search --limit=1 --classname surf    windowactivate --sync || surf", NULL };
static const char *jump_start_sxiv[]         = { "sh", "-c", "xdotool search --limit=1 --classname sxiv    windowactivate --sync || sxiv -rotsf \"`xdg-user-dir SXIV`\"", NULL };
static const char *jump_start_sylpheed[]     = { "sh", "-c", "xdotool search --limit=1 --classname sylpheed windowactivate --sync || sylpheed", NULL };
//static const char *jump_start_thunderbird[]  = { "sh", "-c", "xdotool search --limit=1 --classname thunderbird windowactivate --sync || thunderbird", NULL };
static const char *jump_start_wireshark[]    = { "sh", "-c", "xdotool search --limit=1 --classname wireshark windowactivate --sync || wireshark", NULL };

// A previous concept had key combinations to jump to an application, but not start it if it was not already running. This had limited utility, but could be re-enabled if use cases arise.
//static const char *jump_cantata[]      = { "sh", "-c", "xdotool search --limit=1 --classname cantata windowactivate --sync", NULL };

static const char *play_selection_in_english_espeak[]  = { "sh", "-c", "xsel -po | fold -sw 51 | espeak -venglish", NULL };
static const char *play_selection_in_german_espeak[]   = { "sh", "-c", "xsel -po | fold -sw 51 | espeak -vgerman+whisper", NULL };
// For some reason using named key 'Insert' fails with "st", while its keycode 118 does work.
static const char *start_clipmenu_and_paste[]          = { "sh", "-c", "clipmenu -i && xdotool key --clearmodifiers 'Shift_L+118'", NULL };
static const char *paste_and_clear_selections[]        = { "sh", "-c", "xsel --primary | (sleep 0.1 && xdotool type --clearmodifiers --file -) && (sleep 0.1 && xdotool key --clearmodifiers Tab) && xsel --secondary | (sleep 0.1 && xdotool type --clearmodifiers --file -) && xsel --clear --secondary && xsel --clear --primary && (sleep 0.7 && xdotool --clearmodifiers key Return)", NULL };
static const char *show_qr_code_of_selection[]         = { "sh", "-c", "xsel -po | qrencode --size=20 --output=- | feh --image-bg black --scale-down --auto-zoom --fullscreen -", NULL };
static const char *show_selection_in_sent[]            = { "sh", "-c", "xsel -po | fold -sw 51 | sent -", NULL };
//static const char *start_keynav[]                      = { "sh", "-c", "keynav", NULL };
static const char *start_xkill[]                       = { "sh", "-c", "xkill", NULL };
static const char *logout[]                            = { "sh", "-c", "pkill -u \"$USER\"", NULL };

static const char *tmux_copy_mode[]           = { "sh", "-c", "tmux copy-mode && tmux show-buffer | xsel -ib", NULL };
static const char *tmux_paste_buffer[]        = { "tmux", "paste-buffer", NULL };
static const char *tmux_next_window[]         = { "tmux", "next-window", NULL };
static const char *tmux_previous_window[]     = { "tmux", "previous-window", NULL };
static const char *tmux_split_vertically[]    = { "sh", "-c", "tmux split-window -v -c '#{pane_current_path}' &&"
	"xdotool search --limit=1 --classname alacritty windowactivate --sync ||"
	"xdotool search --limit=1 --classname urxvt windowactivate --sync", NULL };
static const char *tmux_split_horizontally[]  = { "sh", "-c", "tmux split-window -h -c '#{pane_current_path}' && xdotool search --limit=1 --classname alacritty windowactivate --sync || xdotool search --limit=1 --classname urxvt windowactivate --sync",  NULL };
//static const char *tmux_select_1[]            = { "sh", "-c", "tmux select-window -Tt :1", NULL };
//static const char *tmux_select_2[]            = { "sh", "-c", "tmux select-window -Tt :2", NULL };
//static const char *tmux_select_3[]            = { "sh", "-c", "tmux select-window -Tt :3", NULL };
//static const char *tmux_select_4[]            = { "sh", "-c", "tmux select-window -Tt :4", NULL };
//static const char *tmux_select_5[]            = { "sh", "-c", "tmux select-window -Tt :5", NULL };
static const char *tmux_toggle_ip[]           = { "sh", "-c", "beast_desktop_toggle_window 'IP'", NULL };
static const char *tmux_toggle_ledges[]       = { "sh", "-c", "beast_desktop_toggle_window 'Ledges'", NULL };
static const char *tmux_toggle_mp[]           = { "sh", "-c", "beast_desktop_toggle_window 'MP'", NULL };
static const char *tmux_toggle_mutt[]         = { "sh", "-c", "beast_desktop_toggle_window 'MUTT'", NULL };
static const char *tmux_toggle_six[]          = { "sh", "-c", "beast_desktop_toggle_window 'Tongues' '+1'", NULL };
static const char *tmux_toggle_tongues[]      = { "sh", "-c", "beast_desktop_toggle_window 'Tongues'", NULL };
static const char *tmux_toggle_vi[]           = { "sh", "-c", "beast_desktop_toggle_window 'VI'", NULL };
static const char *tmux_toggle_yt[]           = { "sh", "-c", "beast_desktop_toggle_window 'YT'", NULL };
static const char *tmux_toggle_zsh[]          = { "sh", "-c", "beast_desktop_toggle_window 'VI' '-1'", NULL };

/* Handy helpers */
// These click events currently don't work
static const char *mouse_click_left[]              = { "sh", "-c", "xdotool click 1", NULL };
static const char *mouse_click_middle[]            = { "sh", "-c", "xdotool click 2", NULL };
static const char *mouse_click_right[]             = { "xdotool click 3", NULL };
// Copy & paste previous tmux line
static const char *tmux_paste_previous_line[]      = { "sh", "-c", "tmux copy-mode && sleep 0.5 && (xdotool key Up & wait $!) && (xdotool key 0 key 0 key space & wait $!) && (xdotool type $ & wait $!) && (xdotool type $ & wait $!) && (xdotool key h key Return & wait $!) && tmux paste-buffer", NULL };
static const char *tmux_paste_last_word_of_previous_line[]  = { "sh", "-c", "tmux copy-mode && sleep 0.5 && (xdotool key Up & wait $!) && (xdotool type $ & wait $!) && (xdotool type $ & wait $!) && (xdotool key B key space & wait $!) && (xdotool type $ & wait $!) && (xdotool key h key Return & wait $!) && tmux paste-buffer", NULL };
static const char *open_current_wallpaper[]        = { "sh", "-c", "sxiv_browser \"`cat /tmp/beast_desktop_randomize_wallpaper.${USER}`\"", NULL };
// TODO implement writing -previous file
static const char *revert_to_previous_wallpaper[]  = { "sh", "-c", "feh --bg-fill \"`cat /tmp/beast_desktop_randomize_wallpaper.${USER}-previous`\"", NULL };
static const char *raise_arbitrary_counter[]       = { "sh", "-c", "beast_desktop_raise_arbitrary_counter", NULL };
static const char *randomize_wallpaper[]           = { "sh", "-c", "beast_desktop_randomize_wallpaper", NULL };
static const char *randomize_wallpaper1[]          = { "sh", "-c", "beast_desktop_randomize_wallpaper \"`xdg-user-dir WALLPAPER1`\" recursive", NULL };
static const char *randomize_wallpaper2[]          = { "sh", "-c", "beast_desktop_randomize_wallpaper \"`xdg-user-dir WALLPAPER2`\" recursive", NULL };

/* Screenshots */ 
static const char *screenshot_active_window[]      = { "sh", "-c", "beast_desktop_screenshot active_window", NULL };
static const char *screenshot_fullscreen[]         = { "sh", "-c", "beast_desktop_screenshot fullscreen", NULL };
static const char *screenshot_cursor_selection[]   = { "sh", "-c", "beast_desktop_screenshot cursor_selection", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */

	/* Keysyms instead of key characters are used here because the position of the physical keys independent of any keyboard layout is what is relevant.
	 * Use `xev | grep keysym` to find them. As a comment the corresponding key char on a qwerty keyboard is given. */

        /* Audio & Remote Audio */
	{ Mod1Mask|Mod4Mask,            0x67,       spawn,         {.v = increase_volume } },            // i
        { Mod1Mask|Mod4Mask|ControlMask,0x67,       spawn,         {.v = remote_mpd_increase_volume } }, // i
	{ Mod1Mask|Mod4Mask,            0x68,       spawn,         {.v = decrease_volume } },            // u
        { Mod1Mask|Mod4Mask|ControlMask,0x68,       spawn,         {.v = remote_mpd_decrease_volume } }, // u

	{ Mod1Mask|Mod4Mask,            0x6e,       spawn,         {.v = mpv_or_mpd_toggle } },          // j
        { Mod1Mask|Mod4Mask|ControlMask,0x6e,       spawn,         {.v = remote_mpd_toggle } },          // j
	{ Mod1Mask|Mod4Mask,            0x71,       spawn,         {.v = mpv_or_mpd_stop } },            // p
        { Mod1Mask|Mod4Mask|ControlMask,0x71,       spawn,         {.v = remote_mpd_stop } },            // p
        // TODO add shift-variants for more precise seeking
	{ Mod1Mask|Mod4Mask,            0x72,       spawn,         {.v = mpv_or_mpd_seek_back30 } },     // k
        { Mod1Mask|Mod4Mask|ControlMask,0x72,       spawn,         {.v = remote_mpd_seek_back30 } },     // k
	{ Mod1Mask|Mod4Mask,            0x74,       spawn,         {.v = mpv_or_mpd_seek_forward30 } },  // l
        { Mod1Mask|Mod4Mask|ControlMask,0x74,       spawn,         {.v = remote_mpd_seek_forward30 } },  // l
	{ Mod1Mask|Mod4Mask,            0x2c,       spawn,         {.v = mpv_or_mpd_seek_back10 } },     // <
        { Mod1Mask|Mod4Mask|ControlMask,0x2c,       spawn,         {.v = remote_mpd_seek_back10 } },     // >
	{ Mod1Mask|Mod4Mask,            0x2e,       spawn,         {.v = mpv_or_mpd_seek_forward20 } },  // >
        { Mod1Mask|Mod4Mask|ControlMask,0x2e,       spawn,         {.v = remote_mpd_seek_forward20 } },  // >
	{ Mod1Mask|Mod4Mask,            0x64,       spawn,         {.v = mpv_or_mpd_next } },            // :
        { Mod1Mask|Mod4Mask|ControlMask,0x64,       spawn,         {.v = remote_mpd_next } },            // :
	{ Mod1Mask|Mod4Mask,            0x6d,       spawn,         {.v = volume_mute } },                // m
	{ Mod1Mask|Mod4Mask,            XK_Left,    spawn,         {.v = mpv_or_mpd_prev } },
        { Mod1Mask|Mod4Mask|ControlMask,XK_Left,    spawn,         {.v = remote_mpd_prev } },
	{ Mod1Mask|Mod4Mask,            XK_Right,   spawn,         {.v = mpv_or_mpd_next } },
        { Mod1Mask|Mod4Mask|ControlMask,XK_Right,   spawn,         {.v = remote_mpd_next } },
	{ Mod1Mask|Mod4Mask,            XK_Return,  spawn,         {.v = mpd_show_current_id3 } },

        { 0,                            0x1008ff12, spawn,         {.v = volume_mute } },                // XF86AudioMute
        { 0,                            0x1008ff13, spawn,         {.v = increase_volume } },
        { 0,                            0x1008ff11, spawn,         {.v = decrease_volume } },

        { 0,                            0x1008ff02, spawn,         {.v = increase_brightness } },        // XF86MonBrightnessUp
        { 0,                            0x1008ff03, spawn,         {.v = decrease_brightness } },        // XF86MonBrightnessDown

        /* Rii mini remote */
	{ 0,                            0x1008ff17, spawn,         {.v = mpv_or_mpd_next } },            // XF86AudioNext
	{ 0,                            0x1008ff14, spawn,         {.v = mpv_or_mpd_toggle } },          // XF86AudioPlay
	{ 0,                            0x1008ff16, spawn,         {.v = mpv_or_mpd_prev } },            // XF86AudioPrev
        { 0,                            0x1008ff19, spawn,         {.v = tmux_toggle_mutt } },           // XF86Mail
  	{ 0,                            0x1008ff1b, spawn,         {.v = tmux_toggle_mp } },             // XF86Search
        { 0,                            0x1008ff18, spawn,         {.v = tmux_toggle_zsh } },            // XF86HomePage

        /* Jump to or start Applications */
	{ Mod4Mask,                     0x75,       spawn,         {.v = tmux_toggle_ip } },             // a
	{ Mod4Mask,                     0x69,       spawn,         {.v = tmux_toggle_mp } },             // s
	{ Mod4Mask,                     0x61,       spawn,         {.v = tmux_toggle_yt } },             // d
	{ Mod4Mask,                     0x65,       spawn,         {.v = tmux_toggle_ledges } },         // f
	{ Mod4Mask,                     0x6f,       spawn,         {.v = tmux_toggle_tongues } },        // g
	{ Mod4Mask,                     0x73,       spawn,         {.v = tmux_toggle_six } },            // h
	{ Mod4Mask,                     0x6e,       spawn,         {.v = tmux_toggle_zsh } },            // j
	{ Mod4Mask,                     0x72,       spawn,         {.v = tmux_toggle_vi } },             // k
	{ Mod4Mask,                     0x74,       spawn,         {.v = tmux_toggle_mutt } },           // l
	{ Mod4Mask,                     0x64,       spawn,         {.v = jump_start_surf } },            // :

	{ Mod4Mask,                     0x2c,       spawn,         {.v = tmux_previous_window } },       // <
	{ Mod4Mask,                     0x2e,       spawn,         {.v = tmux_next_window } },           // >
	{ Mod4Mask,                     0x6a,       spawn,         {.v = tmux_split_vertically } },      // -
	{ Mod4Mask,                     0x6c,       spawn,         {.v = tmux_split_horizontally } },    // e
	{ Mod4Mask,                     0x70,       spawn,         {.v = tmux_paste_buffer } },          // v
	{ Mod4Mask,                     0x76,       spawn,         {.v = tmux_copy_mode } },             // w

        /* Handy helpers */
	{ Mod1Mask,                     0xe4,       spawn,         {.v = mouse_click_right } },          // c
	{ Mod1Mask,                     0xf6,       spawn,         {.v = mouse_click_middle } },         // x
	{ Mod1Mask,                     0xfc,       spawn,         {.v = mouse_click_left } },           // z
        { Mod1Mask,                     0x65,       spawn,         {.v = play_selection_in_english_espeak } }, // f
        { Mod1Mask,                     0x67,       spawn,         {.v = play_selection_in_german_espeak } },  // i
        { Mod1Mask,                     0x6e,       spawn,         {.v = paste_snippet } },              // j
        { Mod1Mask,                     0x71,       spawn,         {.v = show_qr_code_of_selection } },  // p
        { Mod1Mask,                     0x72,       spawn,         {.v = start_clipmenu_and_paste } },   // k
        { Mod1Mask,                     0x76,       spawn,         {.v = paste_and_clear_selections } }, // w
	{ Mod1Mask,                     0x73,       spawn,         {.v = tmux_paste_last_word_of_previous_line } }, // h
        { Mod1Mask,                     0xdf,       spawn,         {.v = show_selection_in_sent } },     // {
        { Mod1Mask,                     0x79,       spawn,         {.v = tmux_paste_previous_line } },   // "

	{ Mod4Mask,                     0x62,       spawn,         {.v = jump_start_browser } },         // n
     //	{ Mod4Mask|ControlMask,         0x62,       spawn,         {.v = jump_start_chrome } },          // n
     //	{ Mod4Mask,                     0x63,       spawn,         {.v = jump_start_conky } },           // r
     // { Mod4Mask,                     0x66,       spawn,         {.v = jump_feh } },                   // o
     // { Mod4Mask|ControlMask,         0x66,       spawn,         {.v = jump_start_feh } },             // o
     // { Mod4Mask,                     0x66,       spawn,         {.v = jump_deluge } },                // o
     // { Mod4Mask|ControlMask,         0x66,       spawn,         {.v = jump_start_deluge } },          // o
     // { Mod4Mask,                     0x66,       spawn,         {.v = jump_start_firefox } },         // o
     //	{ Mod4Mask|ControlMask,         0x66,       spawn,         {.v = jump_start_firefox } },         // o
     // { Mod4Mask,                     0x67,       spawn,         {.v = jump_start_gmpc } },            // i
     // { Mod4Mask,                     0x67,       spawn,         {.v = jump_cantata } },               // i
     //	{ Mod4Mask|ControlMask,         0x67,       spawn,         {.v = jump_start_cantata } },         // i
	{ Mod4Mask|ControlMask,         0x68,       spawn,         {.v = jump_start_shotcut } },         // u
     // { Mod4Mask,                     0x6b,       spawn,         {.v = jump_start_keepassx } },        // y
     // { Mod4Mask,                     0x6c,       spawn,         {.v = jump_start_libreoffice } },     // e
	{ Mod4Mask,                     0x6d,       spawn,         {.v = jump_start_mpv } },             // m
     //	{ Mod4Mask|ControlMask,         0x6d,       spawn,         {.v = start_mpv } },                  // m
     //	{ Mod4Mask,                     0x70,       spawn,         {.v = jump_start_mupdf } },           // v
        { Mod4Mask,                     0x71,       spawn,         {.v = jump_start_sxiv } },            // p
     // { Mod4Mask|ControlMask,         0x71,       spawn,         {.v = jump_start_sxiv } },            // p
     //	{ Mod4Mask,                     0x76,       spawn,         {.v = jump_start_virtualbox } },      // w
	{ Mod4Mask,                     0x77,       spawn,         {.v = jump_start_wireshark } },       // t
	{ Mod4Mask,                     0x79,       spawn,         {.v = jump_start_sylpheed } },        // "

     // { Mod4Mask|ControlMask,         XK_space,   spawn,         {.v = start_keynav } },

        /* Screenshots */
  	{ 0,                            XK_Print,   spawn,         {.v = screenshot_active_window } },
  	{ ShiftMask,                    XK_Print,   spawn,         {.v = screenshot_fullscreen } },
  	{ Mod1Mask,                     XK_Print,   spawn,         {.v = screenshot_cursor_selection } },

     //	{ Mod1Mask,                     XK_Left,    spawn,         {.v = decrease_volume } },
     //	{ Mod1Mask,                     XK_Right,   spawn,         {.v = increase_volume } },
	{ Mod1Mask,                     XK_Delete,  spawn,         {.v = start_xkill } },
       	{ Mod1Mask,                     XK_F1,      spawn,         {.v = execute_f1_action } },
       	{ Mod1Mask,                     XK_F2,      spawn,         {.v = execute_f2_action } },
	{ Mod1Mask,                     XK_F3,      spawn,         {.v = execute_f3_action } },
	{ Mod1Mask,                     XK_F4,      spawn,         {.v = execute_f4_action } },
	{ Mod1Mask,                     XK_F5,      spawn,         {.v = execute_f5_action } },
	{ Mod1Mask,                     XK_F6,      spawn,         {.v = execute_f6_action } },
	{ Mod1Mask,                     XK_F7,      spawn,         {.v = execute_f7_action } },
	{ Mod1Mask,                     XK_F8,      spawn,         {.v = execute_f8_action } },
	{ Mod1Mask,                     XK_F9,      spawn,         {.v = execute_f9_action } },
	{ Mod1Mask,                     XK_F10,     spawn,         {.v = execute_f10_action } },
	{ Mod1Mask,                     XK_F11,     spawn,         {.v = execute_f11_action } },
	{ Mod1Mask,                     XK_F12,     spawn,         {.v = execute_f12_action } },

        /* Window opacity/transparency */
        { Mod1Mask,                     XK_Prior,   spawn,         {.v = decrease_opacity } },
        { Mod1Mask,                     XK_Next,    spawn,         {.v = increase_opacity } },

	{ Mod1Mask,                     XK_End,     spawn,         {.v = logout } },
	{ Mod1Mask|ShiftMask,           XK_End,     spawn,         {.v = reboot } },
	{ Mod4Mask,                     XK_End,     spawn,         {.v = poweroff } },
        /* On Kineses Freestyle, "Pause" is a function key (blue) on "Insert", so this will make it work without pressing Fn first */
     // { 0,                            XK_Insert,  spawn,         {.v = lock_screen } },
     // { Mod1Mask|ShiftMask,           XK_Insert,  spawn,         {.v = reboot } },
     // { Mod4Mask,                     XK_Insert,  spawn,         {.v = poweroff } },
        { 0,                            XK_Pause,   spawn,         {.v = lock_screen } },
	{ Mod1Mask,                     XK_Pause,   spawn,         {.v = lock_and_suspend_to_ram } },
        { 0,                            XK_Menu,    spawn,         {.v = raise_arbitrary_counter } },

        /* dwm default bindings */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */

        /* Wallpapers */
        { ClkRootWin,           Mod1Mask,          Button1,     spawn,          {.v = randomize_wallpaper } },
        { ClkRootWin,           Mod1Mask,          Button2,     spawn,          {.v = randomize_wallpaper2 } },
        // NOTE The actually desired Button9 is not declared/available:
        { ClkRootWin,           Mod1Mask,          Button3,     spawn,          {.v = open_current_wallpaper } },
        { ClkRootWin,           Mod1Mask,          Button4,     spawn,          {.v = randomize_wallpaper1 } },
        { ClkRootWin,           Mod1Mask,          Button5,     spawn,          {.v = revert_to_previous_wallpaper } },

        /* Audio */
        { ClkLtSymbol,          Mod1Mask|Mod4Mask, Button4,     spawn,          {.v = increase_volume } },
        { ClkStatusText,        Mod1Mask|Mod4Mask, Button4,     spawn,          {.v = increase_volume } },
        { ClkWinTitle,          Mod1Mask|Mod4Mask, Button4,     spawn,          {.v = increase_volume } },
        { ClkClientWin,         Mod1Mask|Mod4Mask, Button4,     spawn,          {.v = increase_volume } },
        { ClkRootWin,           Mod1Mask|Mod4Mask, Button4,     spawn,          {.v = increase_volume } },
        { ClkTagBar,            Mod1Mask|Mod4Mask, Button4,     spawn,          {.v = increase_volume } },

        { ClkLtSymbol,          Mod1Mask|Mod4Mask, Button5,     spawn,          {.v = decrease_volume } },
        { ClkStatusText,        Mod1Mask|Mod4Mask, Button5,     spawn,          {.v = decrease_volume } },
        { ClkWinTitle,          Mod1Mask|Mod4Mask, Button5,     spawn,          {.v = decrease_volume } },
        { ClkClientWin,         Mod1Mask|Mod4Mask, Button5,     spawn,          {.v = decrease_volume } },
        { ClkRootWin,           Mod1Mask|Mod4Mask, Button5,     spawn,          {.v = decrease_volume } },
        { ClkTagBar,            Mod1Mask|Mod4Mask, Button5,     spawn,          {.v = decrease_volume } },

	/* Toggle todo.textile */
        { ClkStatusText,        0,              Button1,        spawn,          {.v = tmux_toggle_vi } },

	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

