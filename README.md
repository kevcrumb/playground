## The Split Linux packages collection

Files required for building and running Split Linux;

### Building

For security reasons the actual creation and signing of packages is currently done locally, considering the CI as untrusted and hence not providing it a trusted private key.

This outlines the process when run as root:


    # Set these
    NAME=
    EMAIL=
    
    xbps-install -Suy
    xbps-install -Sy make bash openssh
    useradd k
    chown -R k:root ./
    su k -c './xbps-src binary-bootstrap
             for package in srcpkgs/beast*; do ./xbps-src pkg `basename $package` ; done
             for package in srcpkgs/splitlinux-*; do ./xbps-src pkg `basename $package` ; done
             [ -f /tmp/id_rsa ] || ssh-keygen -t rsa -m PEM -f /tmp/id_rsa
             xbps-rindex --sign     --privkey /tmp/id_rsa --signedby "${NAME} <${EMAIL}>" hostdir/binpkgs/
             xbps-rindex --sign-pkg --privkey /tmp/id_rsa --signedby "${NAME} <${EMAIL}>" hostdir/binpkgs/*.xbps
	     '

The newly signed packages and the `-repodata` file then has to be added to this very repository under `binpkgs/`.

When pushed to GitLab the package repository will be published according to `.gitlab-ci.yml`.


Refer to [void-linux/void-packages](https://github.com/void-linux/void-packages) for further documentation.
